const chalk = require('chalk')
const msgPath = process.env.GIT_PARAMS
const msg = require('fs').readFileSync(msgPath, 'utf-8').trim()

const commitRE =
  /^(revert: )?(feat|fix|docs|dx|style|refactor|perf|test|workflow|build|ci|chore|types|wip|release)(\(.+\))?(.{1,10})?: .{1,50}/
const mergeRe = /^(Merge pull request|Merge branch)/

if (!commitRE.test(msg)) {
  if (!mergeRe.test(msg)) {
    console.log(msg)
    console.error(
      `${chalk.bgRed.white('错误')}: ${chalk.red(`无效的提交消息格式。`)}
      ${chalk.red(`自动生成更改日志需要正确的提交消息格式。`)}
      例子:
      ${chalk.green(`feat(compiler): add 'comments' option`)}
      ${chalk.green(`fix(v-model): handle events on blur (close #28)`)}
      ${chalk.red(
        `See https://github.com/vuejs/vue-next/blob/master/.github/commit-convention.md for more details.`,
      )}
      `,
    )
    process.exit(1)
  }
}
