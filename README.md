# Tasking 拆解

## 单机

- [x] 用户进入游戏的时候可以看到游戏开始页面
- [ ] 用户点击 startGame 可以开始游戏
- [ ] 用户在开始游戏时候可以看到掉落的方块
- [ ] 方块掉落到最下面边界时候就会停下来
- [ ] 方块掉落到其他地方的时候也会停下来
- [ ] 方块掉落的停下来的时候就会有新的方块掉落下来
  - [ ] 新的方块是随机产生的
- [ ] 用户可以操作方向键盘让正在掉落的方块移动，但是不会超过边界
  - [ ] 左方向键向左
  - [ ] 右方向键向右
- [ ] 用户用方块凑满了一行的话，会消除当前凑满的行，并且会看到上面的行会掉落下来
- [ ] 当方块超出最上面边界的时候，用户会看到游戏结束的提示
- [ ] 用户可以操作空格键来旋转正在掉落的方块
- [ ] 用户可以操作方向键下，来加速正在掉落的方块掉落的速度

## 联机

- [ ] 用户可以看到对手的游戏界面
- [ ] 用户通过对手的游戏界面看到的掉落的方块需要和对手正在掉落的方块一样
- [ ] 用户可以看到对手的所有游戏操作
  - [ ] 方块的向下移动
  - [ ] 方块向左移动
  - [ ] 方块向右移动
  - [ ] 方块旋转
- [ ] 用户消行了，对手会增加一行（这行不可以被消除）
- [ ] 用户游戏结束了，对手会收到游戏获胜的提示

## 双人对战

### 同步的动作

- gameOver (游戏结束)
  - to other
    - gameWon
- eliminateLine (消除行)
  - to self
    - syncAddLine (同步 dival 视图)
  - to other
    - addLine (让其他玩家加行)
- moveBoxToDown (向下移动 box)
  - to other
    - moveBoxToDown
- moveBoxToLeft (向左移动 box)
  - to other
    - moveBoxToLeft
- moveBoxToRight (向右移动 box)
  - to other
    - moveBoxToRight
- rotateBox (旋转 box)
  - to other
    - rotateBox
- createBox (创建 box)
  - to other
    - createBox
